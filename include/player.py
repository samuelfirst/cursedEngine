"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

# Set up player traits w/ defaults
ID = 'PLAYER'
MODEL = '@'
POSITION = [0, 0]
HEALTH = 100
COLOR = ['white', 'black']


# Set up player character
def createPlayer(pid, model, position, health, **kwargs):
    global ID, MODEL, POSITION, HEALTH, COLOR

    ID = pid
    MODEL = model
    POSITION = position
    HEALTH = health
    COLOR = kwargs.get('color', ['white', 'black']) 

# Change player's position to x, y
def movePlayer(x,y):
    global POSITION

    POSITION = [x, y]

# Subtract from player health
def injurePlayer(damage):
    global HEALTH

    HEALTH -= damage

# Add to player health
def healPlayer(health):
    global HEALTH

    HEALTH += health

# Change Player's Model
def changeModel(model):
    global MODEL

    MODEL = model
