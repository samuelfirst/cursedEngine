"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

# Return collisions of passed objects
# Objects are passed as nested lists
# e.x. [ [x , [1,2] ] , [y , [1,2] ] ]
def getCollisions(items):
    collisions = []
    itemCounter = 0

    # Loop over items, except for last item
    # Append colliding items to collisions
    for item in items:
        if item != items[len(items) - 1]:
            itemCounter += 1
            for i in items[itemCounter:]:
                if item[1] == i[1]:
                    collisions.append( [item[0], i[0]] )

    return collisions
