"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

BOXES = []
X = 0
Y = 0

# Create Dialog Box to be passed to renderer
# Dialog Boxes are surrounded by '-', '+', and '|' characters
# Support for multiple dialog boxes will be added in future releases
# Color support may also be added
def createDialogBox(dialogString, x1, y1, x2, y2):
    global BOXES, X, Y

    # Define length and width && border
    length = y2 - y1
    width = x2 - x1
    border = '+' + '-'*(width - 2) + '+'

    # Split dialog on new lines so there can be multi-line dialog boxes
    dialog = dialogString.split("\n")

    # Add starting/ending characters to each line and pad lines out with
    #  white-space
    lineNumber = 0
    for line in dialog:
        line = '|' + line

        if len(line) < width - 1:
            for i in range(width - 1 - len(line)):
                line += ' '
        line += '|'

        dialog[lineNumber] = line
        lineNumber += 1

    # Add top and bottom of dialog box && pad out white space
    dialog = [border] + dialog
    if len(dialog) <= (length - 1):
        for i in range(length - 1 - len(dialog)):
            dialog.append('|' + ' '*(width - 2) + '|')

    dialog.append(border)

    # Add dialog box to BOXES
    BOXES.append(dialog)
    X = x1
    Y = y1

def clearDialog():
    global BOXES
    BOXES = []
