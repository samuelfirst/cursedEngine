"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

# Set up default id, model && list of wall positions
ID = 'WALL'
MODEL = '#'
COLOR = ['white','black']
positions = []

# Add positions for vertical wall
def createVertical(x, y1, y2):
    for y in range(y1, y2+1):
        positions.append( [x,y] )

# Add positions for horizontal wall
def createHorizontal(y, x1, x2):
    for x in range(x1, x2+1):
        positions.append( [x,y] )

def changeColor(color):
    global COLOR

    COLOR = color
