"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

# 1st: get current subsection of map
# 2nd: figure out which npcs/items are in subsection
# 3rd: check list of color pairs to see if current pair
#      has been used && if not add it to the list
#      0 should be [white, black]
# 4th: create list containing npc/item model && color code
# 5th: convert the subsection into lists of characters &&
#      color codes (The same as with npcs/items)
# 6th: go through subsection and add npcs at the correct places
# 7th: export lists of color codes && updated subsection
# 8th: ???
# 9th: profit
# -------------------------------------------------------------
# When it's passed to the engine, the color codes will be
# initialized as color pairs, which will be sent to curses
# along with the characters

# These will be passed to the main body of the engine
# Default color is white on black
COLOR_CODES = [[0, ['white','black']]]
FRAME = []

# Dialog Box information
DIALOG_BOXES = []
DIALOG_X = 0
DIALOG_Y = 0

# Render each game frame and pass it to main
# Note: This will be broken up into sub-functions
#       in the future
def render(subsection, offset, npcItems, player):
    global COLOR_CODES, FRAME

    # Determine where npcs/items are in subsection
    inSubsection = []
    for obj, pos, color in npcItems:
        shiftedPos = [pos[0] - offset[0], pos[1] - offset[1]]
        if shiftedPos[0] >= 0 and shiftedPos[1] >= 0:
            inSubsection.append([obj, shiftedPos, color])

    # Generate List of color pairs
    for obj in inSubsection:
        for code in COLOR_CODES:
            if obj[2] not in code:
                if code is COLOR_CODES[len(COLOR_CODES) - 1]:
                    COLOR_CODES.append([len(COLOR_CODES), obj[2]])
                    obj[2] = len(COLOR_CODES) - 1
                    break
            else:
                obj[2] = code[0]
                break

    # Convert map to color pairs
    # Note: in future versions, colored maps will be supported
    #       however, currently only black and white maps
    #       are supported.  This will be left in for legacy maps
    for line in subsection:
        newLine = []
        for tile in line:
            newLine.append([tile,0])
        FRAME.append(newLine)

    # Place npcs and items in subsection
    for obj in inSubsection:
        FRAME[ obj[1][1] ][ obj[1][0] ] = [obj[0],obj[2]]

    # Generate player color pair && place player in subsection
    # Note: in future versions, multiplayer will be supported,
    #       meaning that this will be done in the same way as
    #       for npcItems
    for code in COLOR_CODES:
        if player[2] not in code:
            if code is COLOR_CODES[len(COLOR_CODES) - 1]:
                COLOR_CODES.append([len(COLOR_CODES), player[2]])
                player[2] = len(COLOR_CODES) - 1
                break
        else:
            player[2] = code[0]
            break

    FRAME[ player[1][1] ][ player[1][0] ] = [player[0],player[2]]

    #If dialog boxes exist, add them to the rendered frame
    for box in DIALOG_BOXES:
        y = DIALOG_Y
        for line in box:
            x = DIALOG_X
            for char in line:
                FRAME[y][x] = [char, 0]
                x += 1
            y += 1
