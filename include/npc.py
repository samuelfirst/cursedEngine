"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

class npc(object):
    def __init__(self, ID, model, position, health, onInteract, **kwargs):
        self.ID = ID
        self.model = model
        self.position = position
        self.health = health
        self.onInteract = onInteract
        self.color = kwargs.get('color', ['white','black'])

    # Return Character Model when being cast as a string
    def __str__(self):
        return self.model

    # Runs passed interaction function
    def interact(self):
        self.onInteract()

    # Move Enemy to target location
    def findPath(self, x, y, canMoveVertically, canMoveHorizontally):
        # Move Vertically if able
        if canMoveHorizontally:
            if self.position[0] > x:
                self.position[0] -= 1
            elif self.position[0] < x:
                self.position[0] += 1

        # Move horizontally if able
        if canMoveVertically:
            if self.position[1] > y:
                self.position[1] -= 1
            elif self.position[1] < y:
                self.position += 1

    # Move enemy to x,y coordinates
    def moveEnemy(self, x, y):
        self.position = [x, y]

    # Subtract health from enemy
    def inureEnemy(self, damage):
        self.health -= damage

    # Add health to enemy
    def healEnemy(self, health):
        self.health += health

    # Change model of enemy
    def changeModel(self, model):
        self.model = model

    def changeID(self, ID):
        self.ID = ID
