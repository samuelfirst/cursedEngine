"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

# The map is a list of strings
MAP = []
SUBSECTION = []
SUBSECTION_SIZE = []
OFFSET = []

# Load map from plain-text file
def loadMap(fi):
    global MAP
    
    with open(fi, 'r') as f:
        MAP = []
        for line in f:
            MAP.append(line.strip())

# Load section of map from x1 to x2 && y1 to y2
# Calculate size && offset of subsection
def loadSection(x1, y1, x2, y2):
    global MAP, SUBSECTION, SUBSECTION_SIZE, OFFSET
    
    section = []
    for i in range(y1, y2+1):
            section.append(MAP[i][x1:x2+1])
            
    SUBSECTION = section
    SUBSECTION_SIZE = [x2 - x1, y2 - y1]
    OFFSET = [len(MAP[0]) - (len(MAP[0]) - x1),
              len(MAP) - (len(MAP) - y1)]
   
