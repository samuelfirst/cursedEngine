"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""

class item(object):
    def __init__(self, ID, model, health, position, onInteract, **kwargs):
        self.ID = ID
        self.model = model
        self.health = health
        self.position = position
        self.onInteract = onInteract
        self.color = kwargs.get('color', ['white', 'black'])

    # Returns model if cast as string
    def __str__(self):
        return self.model

    # Runs passed interaction function
    def interact(self):
        self.onInteract()

    # Subtract health from item
    def damage(self, health):
        self.health -= health

    # Add health to item
    def repair(self, health):
        self.health += health

    # Change item position
    def moveItem(self, x, y):
        self.position = [x,y]

    # Change item model
    def changeModel(self, model):
        self.model = model

    def changeID(self, ID):
        self.ID = ID
