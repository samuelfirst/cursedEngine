"""
cursedEngine
Copyright (C) 2018 Samuel First

This file is part of cursedEngine.

    cursedEngine is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cursedEngine is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cursedEngine.  If not, see <https://www.gnu.org/licenses/>.
"""


# Curses handles the actual drawing to the screen
# && the getting keyboard input
import curses

# Backend of the engine handles everything else
from include import (collisionDetection, item, npc,
                     mapLoad, player, renderer, wall,
                     dialog)

RENDER = True     # Whether to draw frame on this loop
KEYMAP = {}       # Dictionary of keys && associated functions
OBJECT_LIST = []  # List of [game objects, positions, colors]
PLAYER = []       # List containing Player data
EXIT = False      # Exit the game

# Main loop:
#  1. Capture keystrokes && run associated functions
#   1a. If keystroke invalid return to the top of the loop 
#  2. Load user's main loop function
#  3. Determine whether to render frame
#   3a. If render:
#    3aa. Call renderer
#    3ab. Loop over each item in each row && add character to screen 
def main(stdscr, userMainLoop):
    global RENDER, KEYMAP, OBJECT_LIST, PLAYER, EXIT

    curses.start_color()
    curses.use_default_colors()

    while not EXIT:
        # Reset whether each frame can be rendered
        RENDER = True

        # Get key input and apply mapped functions
        try:
            KEYMAP[str(stdscr.getkey())]()
        except KeyError:
            continue

        # Run the user's main loop
        userMainLoop()

        # Render frame, initialize color pairs, draw frame
        if RENDER:
            # Pass dialog boxes to renderer
            renderer.DIALOG_BOXES = dialog.BOXES
            renderer.DIALOG_X = dialog.X
            renderer.DIALOG_Y = dialog.Y
            dialog.clearDialog()
            
            renderer.render(mapLoad.SUBSECTION,
                            mapLoad.OFFSET,
                            OBJECT_LIST,
                            PLAYER)

            initColorPairs()
            curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)

            stdscr.clear()

            y = 0
            for row in renderer.FRAME:
                x = 0
                for character in row:
                    stdscr.addstr(y,
                                 x,
                                 character[0],
                                 curses.color_pair(character[1]))
                    x += 1
                y += 1

            stdscr.refresh()
            renderer.FRAME = []
                
        else:
            continue
            

# Initialize curses wrapper around main loop
def start(mainLoop):
    curses.wrapper(main, mainLoop)

# Add color pairs in renderer to curses
# If color unknown, default to white
def initColorPairs():
    # Can't init_pair() w/out this
    curses.start_color()
    curses.use_default_colors()
    
    for pair in renderer.COLOR_CODES:
        fg = None
        bg = None 

        for color in pair[1]:
            col = curses.COLOR_WHITE

            if color is "white":
                col = curses.COLOR_WHITE
            elif color is "black":
                col = curses.COLOR_BLACK
            elif color is "blue":
                col = curses.COLOR_BLUE
            elif color is "cyan":
                col = curses.COLOR_CYAN
            elif color is "green":
                col = curses.COLOR_GREEN
            elif color is "magenta":
                col = curses.COLOR_MAGENTA
            elif color is "red":
                col = curses.COLOR_RED
            elif color is "yellow":
                col = curses.COLOR_YELLOW

            if fg is None:
                fg = col
            else:
                bg = col

        curses.init_pair(pair[0], fg, bg)
